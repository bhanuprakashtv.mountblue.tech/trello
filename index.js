
window.addEventListener("load", init());

function init() {
  body = document.querySelector(".body-section");
  let div = document.createElement("div");
  div.className = "cl-button"
  let btn = document.createElement("button");
  div.appendChild(btn).innerHTML = "add checklist";
  body.appendChild(div);
  btn.addEventListener('click', checklistfunc)
};


function checklistfunc() {
  body = document.querySelector(".body-section")
  $(".cl-input").remove();

  let clInput = document.createElement("section");
  clInput.className = "cl-input"
  let Name = document.createElement("div");
  clInput.appendChild(Name).innerHTML = "Name :";

  let clName = document.createElement("div");
  clName.className = "cl-name"
  let form = document.createElement("form");
  form.id = "checklist-name"
  let input = document.createElement("input");
  input.setAttribute('placeholder', "Checklist")
  form.appendChild(input);

  clName.appendChild(form);

  let btndiv = document.createElement("div");
  let clbtn = document.createElement("button");
  clbtn.id = "submit";
  btndiv.appendChild(clbtn).innerHTML = "Add CheckList";
  clbtn.addEventListener('click', createChecklist)

  clInput.appendChild(Name).innerHTML = "Name";
  clInput.appendChild(clName)
  clInput.appendChild(btndiv)

  body.appendChild(clInput);
  document.getElementsByClassName("cl-input")[0].style.display = "flex";
}

getCard()
//let val=document.getElementById("checklist-name");

function createChecklist() {
  var name = document.getElementById("checklist-name");
  let clName = name.elements[0].value;
  let checklistpost = `https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9/checklists?name=${clName}&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`

  fetch(checklistpost, {
    method: 'post',
  })
    .then(card => card.json())
    .then(data => createList(data))
}


function getCard() {
  let url = "https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f"
  fetch(url)
    .then(card => card.json())
    .then(data => getChecklist(data))
}

function getChecklist(cl) {

  body = document.querySelector(".body-section")
  let clItem = document.createElement("section");
  clItem.className = "cl-item"
  body.appendChild(clItem);
  clItem.textContent = ""

  for (clId of cl.idChecklists) {
    // console.log(clId);
    url = `https://api.trello.com/1/checklists/${clId}?cards=none&checkItems=all&checkItem_fields=name%2C%20nameData%2C%20pos%2C%20state&fields=all&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
    fetch(url)
      .then(card => card.json())
      .then(data => createList(data))
  }
}

function createList(data) {
  //console.log(data);
  const addElement = document.querySelector('.cl-item');
  const div = document.createElement('div');
  div.className = 'checklist';
  div.id = data.id;
  const btn = document.createElement('button');
  const clName = document.createElement('p');
  const additembtn = document.createElement('button');
  div.appendChild(clName).innerHTML = data.name;
  div.appendChild(additembtn).innerHTML = "Add Item";
  div.appendChild(btn).innerHTML = 'X';
  addElement.appendChild(div);
  btn.addEventListener('click', () => {
    deleteChecklist(data);
  });
  additembtn.addEventListener('click', (e) => {
    additembtn.style.display = "none"
    CreateClItem(e, data);
    getItemsList(e, data)
  })
}

function deleteChecklist(checklist) {
  url = `https://api.trello.com/1/checklists/${checklist.id}?key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  alert(checklist.id)
  fetch(url, {
    method: 'delete',
  })
    .then(card => card.json())
    .then(() => {
      let id = document.getElementById(checklist.id);
      $(id).remove();
    })

};


function CreateClItem(e, item) {
  const addElem = e.target.parentNode;
  //  console.log(addElem);
  const cl = document.createElement('div');
  cl.className = 'checklist1';
  const clItem = document.createElement('input');
  clItem.setAttribute("type", "text");
  clItem.id = 'clItem'
  const clItemAddBtn = document.createElement('button');
  cl.appendChild(clItem);
  cl.appendChild(clItemAddBtn).innerHTML = "Add New Item"
  addElem.appendChild(cl);
  clItemAddBtn.addEventListener('click', function (e) {
    openNewClItem(e, item)
  })

}

function openNewClItem(e, item) {
  let id = document.getElementById('clItem');
  let value = e.target.previousSibling.value;
  url = `https://api.trello.com/1/checklists/${item.id}/checkItems?name=${value}&pos=bottom&checked=false&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  id.value = "";
  fetch(url, {
    method: 'post',
  })
    .then(card => card.json())
    .then(data => getItemsList(e, item))
}
function getItemsList(e, item) {
  //console.log(item);
  url = `https://api.trello.com/1/checklists/${item.id}/checkItems?filter=all&fields=name%2C%20nameData%2C%20pos%2C%20state&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  fetch(url)
    .then(card => card.json())
    .then(data =>
      printItems(e, data))
}

function printItems(e, data) {
  $(".checklistItemView").remove();
  for (let i = 0; i < data.length; i++) {
    let checklistItemView = e.target.parentNode.appendChild(document.createElement('div'));
    checklistItemView.className = "checklistItemView"
    checklistItemView.id = data[i].id;
    let checkListItemBoxdiv = checklistItemView.appendChild(document.createElement('div'));
    checkListItemBoxdiv.className = "checkListItemBoxdiv";
    let checkListItemBox = checkListItemBoxdiv.appendChild(document.createElement('input'));
    checkListItemBox.setAttribute('type', 'checkbox');
    checkListItemBox.id = data[i].id;
    console.log(data[i]);

    fetch(`https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9/checkItem/${data[i]['id']}?fields=name%2CnameData%2Cpos%2Cstate&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`)
      .then(itemdata => itemdata.json())
      .then(citem => {
        checkListItemBox.value = citem['state'];
        // console.log(jitem)
        if (citem['state'] == 'complete') {
          checkListItemBox.setAttribute('checked', 'false');
        }
      });

    let checkListItemP = checkListItemBoxdiv.appendChild(document.createElement('small'));
    checkListItemP.appendChild(document.createTextNode(data[i].name));
    let checkListItemdelete = checkListItemBoxdiv.appendChild(document.createElement('small'));
    checkListItemdelete.appendChild(document.createTextNode('X'))
    checkListItemdelete.addEventListener("click", (e) => {
      itemDelete(data[i]);
    })
    console.log(checkListItemBox.checked)
    checkListItemBox.addEventListener("click", checkChecklistItems)
  }
}

function itemDelete(item) {
  url = `https://api.trello.com/1/checklists/${item.idChecklist}/checkItems/${item.id}?key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  //console.log(item);
  fetch(url, {
    method: 'delete',
  })
    .then(code => code.json())
    .then(() => {
      let id = document.getElementById(item.id);
      $(id).remove();
    })

}

function checked(data) {
  let url = `https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9/checkItem/${data.id}?state=complete&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  fetch(url, {
    method: 'put'
  })
}


function checkChecklistItems(e) {
  //let checklistItemView = e.target.parentNode()
  //console.log(checklistItemView);
  let state = '';
  url = `https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9/checkItem/${e.target.id}?fields=name%2CnameData%2Cpos%2Cstate&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`
  fetch(url)
    .then(card => card.json())
    .then(data => {
      console.log(data);
      if (data.state == 'incomplete') {
        state = 'complete'
      } else if (data.state == 'complete') {
        state = 'incomplete'
      }
      return state;
    }).then(data => {
      fetch(`https://api.trello.com/1/cards/5c94a41559d2b03e8b37d5d9/checkItem/${e.target.id}?state=${data}&key=d5721a824f460d1031229192122d42b6&token=a9838f5e722d44b887ee639e9178faf9a8be44c6fd47c798a434c383b2d4229f`, {
        method: 'PUT'
      }).then(data => data.json())
    })
  console.log(state);
}

